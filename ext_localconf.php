<?php
/**
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'Pi1',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'filter, list, show, ajax, newbees, selection, random, request'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'filter, list, show, ajax, newbees, selection, random, request'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'filter',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'filter, ajax'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'filter, ajax'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'list',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'list, ajax'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'ajax'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'show',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'show, ajax'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'ajax'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'form',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'form, send'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'send'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'newbees',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'newbees'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'random',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'random'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'random'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'EXOTEC.Cardealer',
            'selection',
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => 'selection'
            ],
            // non-cacheable actions
            [
                \EXOTEC\Cardealer\Controller\CardealerController::class => ''
            ]
        );


		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        $iconRegistry->registerIcon(
            'cardealer-plugin-cardealer',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:cardealer/Resources/Public/Icons/user_plugin_cardealer.svg']
        );

        // Custom Routing Aspects Mapper
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['routing']['aspects']['IdentifierValueMapper'] = \EXOTEC\Cardealer\Routing\Aspect\IdentifierValueMapper::class;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['routing']['aspects']['LimitValueMapper'] = \EXOTEC\Cardealer\Routing\Aspect\LimitValueMapper::class;

        // register own cache
        if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cardealer_cache'])) {
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cardealer_cache'] = array();
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cardealer_cache']['frontend'] = 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend';
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cardealer_cache']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend';
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cardealer_cache']['options']['compression'] = 1;
        }


        // System email templates
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][800] = 'EXT:cardealer/Resources/Private/Templates';
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['partialRootPaths'][800] = 'EXT:cardealer/Resources/Private/Partials';
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['layoutRootPaths'][800] = 'EXT:cardealer/Resources/Private/Layouts';


    }
);
